from flask import Flask, render_template, url_for, request, redirect
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///asign1ps.db'

class Todo(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.String(200), nullable=False)
	date_created = db.Column(db.DateTime, default=datetime.utcnow)

@app.route ('/')
def saludar():
    return 'Buen dia Profesor, se le mostraran algunas palabras dentro del Diccionario!'

@app.route ('diccionariops')
def diccionariops(diccionariops):
    return 'Chaniao: Persona bien vestida, Bulto: Persona que estorba, Parranda: Fiesta grande.'

@app.route('/hola/<nombre>')
def saludar_nombre(nombre):
    return 'Buenas tardes %s!' % nombre

if __name__ == '__main__':
    app.run()

